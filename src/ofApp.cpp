#include "ofApp.h"
int baud = 9600;

int posX = 0;
int posY = 0;
string targetX = "0";
string targetY = "0";
string updatedPosStream = "";

void ofApp::setup(){
    ofSetVerticalSync(true);
    ofBackground(0);
    
    ofSetLogLevel(OF_LOG_VERBOSE);
    
    //------------------------   Serial setup
    bSendSerialMessage = false;
    
    serial.listDevices();
    vector <ofSerialDeviceInfo> deviceList = serial.getDeviceList();
    //this was a vector from oFx by default to enumerate serial devices
    //with device infos in them <http://openframeworks.cc/documentation/types/ofSerialDeviceInfo/#show_ofSerialDeviceInfo>
    
    //Now create a vector list to store the names of the serial devices
    //Why not string array? as it is not dynamic and you need to predefine the size for memory allocation.
    vector<string> deviceNames;
    
    //going through all the devices from the devicelist vector
    //and putting them in the deviceNames vector list
    for (int i = 0; i < deviceList.size(); i++){
         // pushing back for proper memory allocation and avoiding garbage
         deviceNames.push_back(deviceList[i].getDeviceName());
     }
    
    // use the generated vector array to be used in the GUI drop down element
    // as the  gui.addComboBox class has two signatures one which takes string array with size defined
    // and other that also takes vector list .. Yipeee .. So decided up there to use vector list and
    // ditched the whole string array conversion problem
    
    gui.addTitle("serialDevices");
    gui.addComboBox("serialDevices", serialDevices, deviceNames);
    gui.loadFromXML();
    gui.show();
    
    serial.setup(0, baud); //open the first device
    //serial.setup("/dev/tty.usbserial-A4001JEC", baud); // mac osx example
}


void ofApp::update(){
    
    if (bSendSerialMessage){
        
        
        targetX = ofToString(posX);
        targetY = ofToString(posY);
        
        //        --------   sample protocol string  ----->  xPos>int;yPos:int</
        
        updatedPosStream = "xPos>" + targetX + ";yPos:" +  targetY + "</" ;
        
        unsigned char* stream = (unsigned char*) updatedPosStream.c_str(); // cast from string to unsigned char* buffer array
        
        serial.writeBytes(&stream[0], updatedPosStream.length());
        
        bSendSerialMessage = false;
    }
}


void ofApp::draw(){
    
    ofSetColor(255);
    gui.draw();
    
    ofDrawBitmapString("Serial protcol string: " + updatedPosStream , 10, ofGetWindowHeight() - 20);
    
    ofSetColor(255, 255, 255);
    ofDrawCircle(posX, posY, 10); //is there a smooth() function for drawings?
    ofSetLineWidth(0.05); // doesn't work I don't know why for < 0;
    ofDrawLine(0, posY, ofGetWindowWidth(), posY);
    ofDrawLine(posX, 0, posX, ofGetWindowHeight());
}

void ofApp::mouseDragged(int x, int y, int button){
    bSendSerialMessage = true;
    
    posX = mouseX;
    posY = mouseY;
}
